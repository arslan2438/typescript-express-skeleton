import { Request } from 'express';
import {IUserDoc} from './../users/user.model';

interface RequestWithUser extends Request {
  token: string;
  user: IUserDoc;
}

export default RequestWithUser;
