import * as express from 'express';
import * as _ from 'lodash';
import RequestWithUser from 'interfaces/requestWithUser.interface';
import Controller from '../interfaces/controller.interface';
import validationMiddleware from '../middleware/validation.middleware';
import authenticate from './../middleware/auth.middleware';
import CreateUserDto from '../users/user.dto';
import LogInDto from './logIn.dto';
import {User} from './../users/user.model';

class AuthenticationController implements Controller {
  public path = '/auth';
  public router = express.Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/register`, validationMiddleware(CreateUserDto), this.registration);
    this.router.post(`${this.path}/login`, validationMiddleware(LogInDto), this.loggingIn);
    this.router.delete(`${this.path}/logout`, authenticate, this.loggingOut);
  }

  private registration = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
   try {
    const body = _.pick(request.body, ['email', 'password']);
    const user = new User(body);
    await user.save();
    let token = await user.generateAuthToken(); 
    response.header('x-auth', token).send(user);
   } catch (e) {
    response.status(400).send(e);
   }
  }

  private loggingIn = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
   try {
    const body = _.pick(request.body, ['email', 'password']); 
    const user = await User.findByCredentials(body.email, body.password);
    const token = await user.generateAuthToken();
    response.header('x-auth', token).send(user);
    } catch (e) {
      response.status(400).send(e.message);
   }
  }

  private loggingOut =  async (request: RequestWithUser, response: express.Response) => {
    try {
      await request.user.removeToken(request.token);  
      response.status(200).send();
    } catch (e) {
      response.status(400).send(e);
    }
  }
}

export default AuthenticationController;
