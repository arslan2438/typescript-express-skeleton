import { Document, Schema, Model, model } from 'mongoose';
import * as _ from 'lodash';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import validator from 'validator';
import IUser from './user.interface';

const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: validator.isEmail,
      message: '{VALUE} is not a valid email'
    }
  },
  password: {
    type: String,
    require: true,
  },
  tokens: [{
    access: {
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  }]

});
//An instance Method - for returning only the globle data not the secure data
UserSchema.methods.toJSON = function () {
  let user = this;
  let userObject = user.toObject();
  return _.pick(userObject, ['_id', 'email']);
};

//An instance Method - for hashing/Salting the password 
UserSchema.methods.generateAuthToken = async function () {
  try {
  let user = this;
  let access = 'auth';
  let token = jwt.sign({_id: user._id.toHexString(), access}, process.env.JWT_SECRET).toString();
  user.tokens.push({access, token});
  await user.save();
  return token;
  } catch (e) {
   throw new Error(e); 
  }
};

//An instance Method - for romving a token at the time of logging out
UserSchema.methods.removeToken = function (token:string) {
  let user = this;
  return user.updateOne({
    $pull: {
      tokens: {token}
    }
  })
};

//An Model Method i.e like "findOneAndUpdate()" but UserDefined
UserSchema.statics.findByToken = function (token:string) {
  try {
    let decoded: any = jwt.verify(token, process.env.JWT_SECRET);
    let user = User.findOne({
      '_id': decoded._id,
      'tokens.token': token,
      'tokens.access': 'auth'
    });
    return user;
  } catch (e) {
    return Promise.reject(e);
  }
};

//An Model Method -  for logging in the user
UserSchema.statics.findByCredentials = async function (email:string, password:string) {
  try {
    let user = await User.findOne({email}) ;
    if (!user) {
      throw new Error(`${email} not found`);
    }
    if(bcrypt.compareSync(password, user.password)){
      return user;
    }
    else{
      throw new Error('Password does not match');
    }
  } catch (e) {
    throw new Error(e);
  }
};

//a middleware function that is called before the "save()" executes because of ".pre()"
UserSchema.pre<IUserDoc>('save', function (next) {   
  let user = this;
  if (user.isModified('password')) {
    let salt = bcrypt.genSaltSync(10);
    let hash = bcrypt.hashSync(user.password, salt);
    user.password = hash;
    next();
  }
  else {
    next();
  }
});

export interface IUserDoc extends  IUser, Document {
  generateAuthToken():Promise<string>;
  toJSON():JSON;
  removeToken(token:string):Promise<IUserDoc>;
}

export interface IUserModel extends Model<IUserDoc> {
  findByToken(token:string):Promise<IUserDoc>;
  findByCredentials(email:string, password:string):any;  
}

export const User = model<IUserDoc,IUserModel>('User', UserSchema);
