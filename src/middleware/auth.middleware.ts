import { NextFunction, Response } from 'express';
import RequestWithUser from '../interfaces/requestWithUser.interface';
import {IUserDoc, User} from '../users/user.model';

let authenticate = async (request: RequestWithUser, response: Response, next: NextFunction) => {
  try {
    let token = request.header('x-auth');
    let user = await User.findByToken(token) 
    if (!user) {
      throw new Error('User not found')
    }
    request.user = user;
    request.token = token;
    next();    
  } catch (e) {
    response.status(401).send();
  }

}
export default authenticate;
